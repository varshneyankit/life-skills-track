# REST Architecture

## Introduction

REST stands for Representational State Transfer. It is a way to design network applications using a set of guidelines and principles. REST is commonly used with HTTP. REST is widely used for creating scalable web services.

## Key Principles of REST

REST is based on six key principles:

1. **Client-Server Architecture**: The system is divided into clients and servers. Clients request resources, and servers provide those resources.

2. **Statelessness**: Each client request must contain all the information needed for the server to understand and process it.

3. **Cacheability**: Server responses can be marked as cacheable or non-cacheable. Cacheable responses can be reused by clients, improving performance and reducing server load.

4. **Uniform Interface**: REST uses a consistent interface for communication, making the system simpler and more transparent.

5. **Layered System**: The architecture can have multiple layers, each interacting only with its immediate neighbor. This design supports scalability and flexibility.

6. **Code on Demand**: Servers can extend client functionality by sending executable code, such as JavaScript. This is optional and not widely used.

## Methods

REST uses standard HTTP methods to perform actions on resources:

- **GET**: It is used for retrieving a resource.
- **POST**: It is used for creating a new resource.
- **PUT**: It is used for updating an existing resource.
- **DELETE**: It is used for deleting a resource.
- **PATCH**: It is used for partially updating a resource.

## Designing a RESTful API

The following best practices should be followed while designing a RESTful API.

1. Resource URIs should represent entities, like `/users` for user resources.
2. Use methods like GET for retrieval, POST for creation, PUT for updates, and DELETE for deletions.
3. Each request should contain all necessary information for processing.
4. Appropriate HTTP status codes should be used to indicate the result of an operation.

   - `200 OK` for success.
   - `201 Created` for a new resource.
   - `400 Bad Request` for invalid requests.
   - `404 Not Found` for non-existent resources.
   - `500 Internal Server Error` for server issues.

5. Use HTTPS to encrypt data, implement proper authentication and authorization mechanisms, and validate all inputs to prevent attacks.

## Benefits of REST

- **Scalability**: Statelessness and cacheability helps in scaling applications efficiently.
- **Flexibility**: Separating client and server allows them to evolve independently.
- **Performance**: Caching improves performance by reducing server load and latency.
- **Simplicity**: A uniform interface simplifies the architecture which makes development and maintenance easier.

## Example of a RESTful Service

This is an example of a RESTful service for managing users:

- **GET /users**: This retrieves a list of users.
- **POST /users**: This creates a new user.
- **GET /users/{id}**: This retrieves a specific user by ID.
- **PUT /users/{id}**: This updates a specific user by ID.
- **DELETE /users/{id}**: This deletes a specific user by ID.

## Conclusion

REST is a powerful way to design networked applications. By following its principles, developers can create robust, efficient, and easy-to-maintain web services.

## References

- [Introduction to REST](https://developer.mozilla.org/en-US/docs/Glossary/REST)
- [What is REST](https://www.codecademy.com/article/what-is-rest)
- [REST API for Beginners](https://www.freecodecamp.org/news/what-is-rest-rest-api-definition-for-beginners/)
- [The six Guiding Principles of RESTful Architecture](https://medium.com/@Ian_carson/the-six-guiding-principles-of-restful-architecture-852d707b9036)
- [REST API Tutorial](https://restfulapi.net/)
