# Energy Management

Question-1:  
What are the activities you do that make you relax - Calm quadrant?

Answer:  
Some activities that makes me relax are:

- Walking
- Exercise
- Listening music
- Painting

Question-2:  
When do you find getting into the Stress quadrant?

Answer:  
I find myself in the stress quadrant in the following things:

- Exam studies
- Resolving conflicts
- Public speaking
- Not able to complete work on time

Question-3:  
How do you understand if you are in the Excitement quadrant?

Answer:

Things that make me feel in the excitement quadrant are:

- After doing intense workout.
- After solving more than 2 problems in coding contests.
- Watching movies.
- Playing games.

Question-4:  
Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.

Answer:

- Sleep is very important for our health.
- Sleep helps in remembering things better.
- Sleep maintains health of the heart.
- Taking less sleep causes mental stress.
- Quality sleep improves creativity.

Question-5:  
What are some ideas that you can implement to sleep better?

Answer:

Ideas that I will implement to sleep better are:

- Follow a consistent sleep routine.
- Sleeping in a dark room.
- Limiting screen time before going to bed.
- Doing exercise for a better sleep.

Question-6:  
Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.

Answer:

Brain Changing Benefits of Exercise:

- It makes us feel stronger.
- It has positive effects on our mental health.
- It increases long term memory.
- It increases focus power.
- It helps in remembering things for long time.

Question-7:  
What are some steps you can take to exercise more?

Answer:

Steps that I can take to exercise more are:

- Sticking to a daily exercise routine.
- Making a new gym friend.
- Doing intense workout sessions.
- Prioritizing exercise.
