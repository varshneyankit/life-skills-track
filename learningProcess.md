# Learning Process

## Question 1: What is the Feynman Technique?

Understanding a concept and then teaching it to someone so that it gets hard for you to forget the concept.

## Question 2: In this video, what was the most interesting story or idea for you?

The most interesting idea is to use the Pomodoro technique. Learning a topic in focused mode and then relaxing a little bit is the best way to learn something.

## Question 3: What are active and diffused modes of thinking?

Active mode of thinking is when we focus on a topic intensely for better understanding the topic.
Diffused mode of thinking is a more relaxed state which promotes creativity and new perspectives.

## Question 4: According to the video, what are the steps to take when approaching a new topic? Only mention the points

Four steps to follow for approaching a new topic:

1. Deconstruction of the skill.
2. Learning the skill enough to self-correct.
3. Removing the practice barriers.
4. Practicing the topic for at least 20 hours.

## Question 5: What are some of the actions you can take going forward to improve your learning process?

Actions that I will take:

1. Using the Pomodoro technique to learn something.
2. Learning and then teaching the topic to someone.
3. Using focused and diffused mode for better understanding of a topic.
