# Prevention of Sexual Harassment

## 1.What kinds of behaviour cause sexual harassment?

- Verbal Harassment: Sexual comments, jokes, requests for sexual favors, sexual innuendos, threats, and spreading sexual rumors.

- Visual Harassment: Posters, drawings, pictures, screensavers, cartoons, emails, or texts of a sexual nature.

- Physical Harassment: Sexual assault, blocking movement, inappropriate touching, sexual gestures, and leering or staring.

## 2. What would you do in case you face or witness any incident or repeated incidents of such behaviour?

- If it's safe, step in and address the behavior.
- Offer support and let them know you're there for them.
- Write down details of what you saw.
- Inform a supervisor, HR, or appropriate authority.
