# Tiny Habits

## 1. Tiny Habits - BJ Fogg

Question-1:  
In this video, what was the most interesting story or idea for you?

Answer:  
The most interesting story in BJ Fogg's video is about his friend. His friend wanted to get into the habit of flossing regularly. Instead of trying to floss all of his teeth at once, he started by flossing just one tooth. It was really easy to do, and it made starting the habit simple. Over time, he got used to it and started flossing all of his teeth regularly. This story shows how starting with small habits can turn into bigger ones.

## 2. Tiny Habits by BJ Fogg - Core Message

Question-2:  
How can you use B = MAP to make making new habits easier? What are M, A, and P?

Answer:  
We can make new habits easier using BJ Fogg's B = MAP formula:

- M is for Motivation, which means finding a reason that's important to you to do the habit.
- A is for Ability, which means making the habit really easy to do.
- P is for Prompt, which means using reminders to help you remember to do the habit.

For example, if you want to start exercising, find something fun that motivates you to do it. Start with something simple, like just one push-up. And try doing it right after you brush your teeth so you remember. Doing these things together can make it easier to start new habits.

Question-3:  
Why is it important to "Shine" or celebrate after each successful completion of a habit?

Answer:  
It's important to celebrate after finishing a habit because it makes you feel good. When you feel good about doing something, your brain likes it and wants to do it again. This happy feeling helps your brain learn the habit faster because it becomes something you enjoy and look forward to doing again.

## 3. 1% Better Every Day Video

Question-4:  
In this video, what was the most interesting story or idea for you?

Answer:  
The video talks about a really cool idea which is making small changes in your habits every day can lead to big success in the long run. This means paying attention to little things, like getting a better pillow for sleeping or practicing a skill for just a few minutes each day. These small changes might not seem like much at first, but they really add up over time, just like how a cycling team won the Tour de France by making lots of small improvements.

The video also mentions how our habits affect who we are as people. By changing our habits, we can actually change how we see ourselves and what we believe we're capable of.

## 4. Book Summary of Atomic Habits

Question-5:  
What is the book's perspective about identity?

Answer:  
Our habits are shaped by who we are. We should aim to be the kind of person who naturally has the habits we want.

Question-6:  
Write about the book's perspective on how to make a habit easier to do.

Answer:  
Make the habit easy to start and keep going by simplifying the process and reducing any obstacles.

Question-7:  
Write about the book's perspective on how to make a habit harder to do.

Answer:  
We can make a habit harder by adding obstacles and making it more difficult.

## 5. Reflection

Question-8:  
Pick one habit that you would like to do more of. What are the steps that you can take to make the cue obvious or the habit more attractive or easy and/or the response satisfying?

Answer:  
Exercising daily

- I can put my workout clothes and gear where I can see them easily.
- I can make exercising fun by joining a class I enjoy or listening to my favorite music.
- I can start with short and easy workouts to make it simpler.
- I can keep track of how I am doing and treat myself to something healthy or relaxing as a reward.

Question-9:  
Pick one habit that you would like to eliminate or do less of. What are the steps that you can take to make the cue invisible or the process unattractive or hard or the response unsatisfying?

Answer:  
Reducing time on using screens.

- I can set a time when I should stop using devices.
- I can keep electronic devices out of my bedroom where I can't see them easily.
- I can use apps that limit screen time.
- I can make the screen look less appealing by making it black and white.
