# Listening and Active Communication

## Question 1 - What are the steps/strategies to do Active Listening? (Minimum 6 points)

Steps to do Active Listening are:

1. Pay attention to the speaker and avoid distractions.
2. Do not interrupt while the other person is talking.
3. Use verbal and non-verbal cues to show interest and engagement.
4. Paraphrase the speaker's words to ensure understanding and demonstrate empathy.
5. Taking notes during an important conversation.
6. Use proper body language to show attention towards the speaker.

## Question 2 - According to Fisher's model, what are the key points of Reflective Listening? (Write in your own words, use simple English)

Key points of Reflective Listening are:

1. Listen carefully without any interruption.
2. Try to grasp the speaker's words and feelings.
3. Repeat what the speaker said.
4. Understand the speaker's underlying emotions.
5. Listen without criticizing and judging.
6. Ask questions if something is unclear to ensure full understanding.

## Question 3 - What are the obstacles in your listening process?

The main obstacles in my listening process are distractions and not able to pay attention for longer durations.

## Question 4 - What can you do to improve your listening?

I should do following things:

- Eliminating distractions.
- Staying focused.
- Taking notes.
- Asking questions for better understanding.

## Question 5 - When do you switch to Passive communication style in your day to day life?

I usually tend to switch to passive communication in my day to day life when I want to avoid conflicts, feeling overwhelmed or have little interest in the conversation.

## Question 6 - When do you switch into Aggressive communication styles in your day to day life?

I usually tend to switch to aggressive communication in my day to day life in the following scenarios:

- Whenever I feel frustrated.
- Whenever someone is crossing their boundaries.

## Question 7 - When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

I usually switch to passive aggressive communication when:

- I want to avoid a fight.
- I don't want to have a conversation.
- I don't want to hurt someone.

## Question 8 - How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life? (Watch the videos first before answering this question.)

I would practice the following strategies to make my conversation more assertive:

1. Provide clear and focused communication.
2. Express the emotions driving my goals.
3. Demonstrate empathy in interactions.
4. Remain composed and respect my limits.
5. Assertively convey messages through body language.
