# Good Practices for Software Development

## Which point(s) were new to you?

- Time tracking to improve my productivity while working.
- Knowing team members for better coordination.
- Sending meeting notes to team members.

## Which area do you think you need to improve on? What are your ideas to make progress in that area?

- I need to improve in asking questions when I am stuck. I will try to first explore solutions by myself and then ask for assistance.
- I need to improve in setting boundaries for work time and personal time. I will try to minimize distractions while working so that I can remain focused.
