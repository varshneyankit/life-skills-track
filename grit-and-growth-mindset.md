# Grit and Growth Mindset

## 1. Grit

Question:  
Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.

Answer:  
It talks about grit, which means keeping going and being really passionate about your big goals for a long time.

## 2. Introduction to Growth Mindset

Question:  
Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.

Answer:  
It talks about the growth mindset.  
It's about believing that we can get better at things if we work hard and keep trying.

## 3. Understanding Internal Locus of Control

Question:  
What is the Internal Locus of Control? What is the key point in the video?

Answer:  
It's about believing that we can control what happens in our life. The video says that it's really important to think this way because it helps us stay motivated and succeed.

## 4. How to build a Growth Mindset

Question:  
What are the key points mentioned by the speaker to build growth mindset (explanation not needed).

Answer:

Key points for building Growth Mindset are:

- Don't be afraid of challenges.
- Keep trying even when things go wrong.
- Think of hard work as the way to get really good at something.
- Listen to feedback and learn from it.
- Look at how others succeed and learn from them.

## 5. Mindset - A MountBlue Warrior Reference Manual

Question:  
What are your ideas to take action and build Growth Mindset? in .md format

Answer:  
The ideas for building Growth Mindset are:

- Learn from mistakes.
- Keep learning new things.
- Be happy about the progress you make.
- Think about how you're getting better regularly.
- Think of challenges as chances to get better.
- Hang out with friends who also want to grow.
- Set goals that are big but possible.
- Be kind to yourself and don't be too hard on yourself.
- Ask for feedback and use it to improve.
